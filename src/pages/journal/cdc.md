---
layout: "../../layouts/BlogPost.astro"
title: "Calcutta Debate Circle Application"
description: "An application on the subject of membership written by Aryan Singh to the Calcutta Debate Circle."
pubDate: "January 01, 2016"
heroImage: "https://media.aryan.ink/assets/images/cdc.png"
altImage: "A man having a discussion with a horse"
author: "a115"
authorName: "Aryan Singh"
---
Back in 2013, I accompanied my father to the annual book fair here in Calcutta, Where saw a few painters and an economist having a lively exchange between what the artist was trying the portray in a painting of his, I was hooked - the different messages different humans coming from various walks of life, could draw from the very same source. It somehow felt relatable which was unusual seeing as I didn’t even know what a debate was until that day. After a week of turmoil, It struck me the months ago during a weekend, when I was watching Star Trek Enterprise on a particular episode we saw a few aliens coming up on the Earth Star Ship Enterprise for a tour and were offended when they came into dining area, Since they consider the act of eating a very intimate and prefer to do it in the privacy of their living area, which in deep space is there private quarters. Ever since that month, I have engaged in the activity of Debate, just for the sheer fun of it. And joining the club of like seems like the logical next step.

I hope I was able to convey my message and that whoever is reading this will consider my application!
