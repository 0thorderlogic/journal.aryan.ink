---
layout: "../../layouts/BlogPost.astro"
title: "End of line, 2021"
description: "Annual message from Aryan Singh, towards the end of the year."
pubDate: "December 19, 2021"
heroImage: "https://media.aryan.ink/assets/images/end-of-line-02.png"
altImage: "A man standing in a mountain valley"
author: "a115"
authorName: "Aryan Singh"
---

Tonight is the final call of the year.
The last time I heard them laugh
I clung on tight the last time I got to.
And take this journey with strangers who come out every week.
There were distinct laughs with blurry faces for the one on stage.
I walk up and grab the mic as the waitress brings me my tea at the end of it all.
Good night, Calcutta!