---
layout: "../../layouts/BlogPost.astro"
title: "Such are the days here on earth"
description: "This poem by Aryan Singh was originally written as part of a play in 2019. The play told the story of a man who had lost his love! The main character writes a series of letters informing her about his whereabouts and everyday happenings; this was the final letter he wrote on the night of December 2, 1985—the night he becomes a victim of the Bhopal disaster."
pubDate: "August 17, 2019"
heroImage: "https://media.aryan.ink/assets/images/such-are-the-days-here-on-earth.png"
altImage: "A boy looking at the stars, while siting on the terrace. A scene from sub-urban India"
author: "a115"
authorName: "Aryan Singh"
---

Misty mornings, stargaze nights!<br/>
But not all is in harmony — We are at war! Tomorrow not all will continue,and the ones who remain will never be the same.<br/>
Such are the days here on Earth!<br/>
Her smiles light up the place, there demise brings us closer.<br/>
My father returned home this week, my brother is still working on his dream, my sister sighs over the rolling ring,<br/>
Such are the days here on Earth!<br/>
I am here like a bridge over troubled waters.<br/>