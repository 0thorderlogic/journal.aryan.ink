---
layout: "../../layouts/BlogPost.astro"
title: "What was I to her?"
description: "Too many question, too few answers."
pubDate: "February 4, 2023"
heroImage: "https://media.aryan.ink/assets/images/what-was-i-to-her.png"
altImage: "A man staring down while a woman looks at him with love"
tags: "POETRY"
author: "a115"
authorName: "Aryan Singh"
---

What was I to her?<br/>
What am I to her?<br/>
Will I ever mean anything to her?<br/>
If my friends are to be believed. I won't.<br/>
But alas, I feel I might.<br/>
She might change her mind, she might let him go.<br/>
But alas, I feel she won't.<br/>
Don't feel sorry for us.<br/>
He was a faceless bystander in my life.<br/> 
But in his, I am but a fool.<br/>
Would I ever move past her?<br/>
Why was she this cruel? <br/>
Why won't she let me go?<br/>
But alas, I feel she won't.<br/>
Someday I will simply leave.<br/>
Would she remember me when I am gone?<br/>
But alas, I feel she won't.<br/>