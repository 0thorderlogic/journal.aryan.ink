---
layout: "../../layouts/BlogPost.astro"
title: "I left my soul on the shore"
description: "Remember the day you spent at the beach? I do. This may have been the result of listening to Malibu, a track on the 2017 Album Younger now by Miley Cyrus"
pubDate: "December 09, 2022"
altImage: "A man standing on the beach"
heroImage: "https://media.aryan.ink/assets/images/i-left-my-soul-on-the-shore.png"
author: "a115"
authorName: "Aryan Singh"
---
Waves racing, wind gushing, in chaos in harmony!<br/>
As the star hid behind the horizon, I felt calm.<br/>
The monoliths failed the test of time they now lie broken, and refined.<br/>
Waves rose time and time again, they fell time and time again.<br/>
They swept the bed under me, I felt no urge to fight it.<br/>
As I walked away, I found I left my soul on the shore<br/>