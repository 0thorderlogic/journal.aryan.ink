---
layout: "../../layouts/BlogPost.astro"
title: "How to raise funding for startups"
description: "My notes from a course I did on raising money for startups."
pubDate: "Jan 12, 2023"
altImage: "A stack of money"
heroImage: "https://media.aryan.ink/assets/images/how-to-raise-funding-for-startups.png"
author: "a115"
authorName: "Aryan Singh"
---

## Funding Basics

There are a few types of funding. Amongst them, the most common one is Equity funding.


**What is Equity Funding?**

Your company gets money in return for the equity i.e. you sell a piece of your company to the investor.


Once you incorporate your company. You issue a net of 1,000,000 (1 Million) shares of the incorporated company.


**Types of Equity funding:** 
        
* **Primary:** Here the company gets the money to spend. Not a shareholder or Individual involved in the company.
   * Company gets money
   * Various stages of funding
      * Pre-Seed - By Angel Investors. To get off the ground.
      * Seed - By Angel Investors. To cover running operations
      * Bridge<sup id="one"><a href="#1">[1]</a></sup>
      * Series A - Generally by VCs
      * Series B - [C, D, E,…….., Z] - Generally by VCs
      * Later on Institutional equity investors


* Secondary: Here an individual shareholder/Promoter gets the money.
   * No money gets to the company
   * Company stays hush about it. No media/PR
   * Mini liquidation
   * Can happen at any stage


## Understanding major funding stages

**Pre-Seed:** When you have an Idea ready and a founding team ready to go. 
* Money invested in an Idea
* No team except founders
* MVP (Minimum viable product) or no product
* $50,000 - $250,000
* Friends, Family and Fools. 


**Why do people fail to raise Pre-Seed?**

Generally, when people have an idea, they also need the skills necessary to execute it, for example, you are a personal gym trainer and have an idea for a fitness app which can have some killer new revolutionary features, but have no technical skills to make this.
In this case, you will be looking for a pre-seed round to Hire engineers to make you an MVP and the thing is these engineers are costly when they are working for a salary. So angel investors to steer clear of this. So what you can do to save yourself is to get engineers willing to work for Shares of your Company rather than a salary.


**Investors play with probability and you have to prove yourself as a relatively less risky investment and the best way to portray that is to put your money where your mouth is.**


**Also when people leave their comforting jobs to live this life of struggle as an entrepreneur it shows their commitment to the idea.**


**Seed:** This is money for you to experiment with. This is the round where you raise money to find Product Market Fit. In product market fit you try to answer a few questions. Who needs your product? How much will one pay to use your product? At what frequency will one need your product? What is the potential of your product? Once you answer all these questions you have what in the industry is known as a playbook. You use this model in your playbook to expand and grow your business.
* Have a prototype
* Some customers
* Trending upward metrics
* Small team
* Need money to build product full product
* $150,000 - $2,000,000 (serial entrepreneurs get more!)
* Angles and seed VCs
* If you have no business model and no clear way to monetize, you will need a million plus users to be even taken seriously.


**Series A:** A template is ready. Your product has market acceptance. Market clarity.
* Fully functioning product
* Already have customers
* Revenue/Large user base
* Leadership team who can handle large money to grow
* $3,000,000 - $10,000,000 
* Early growth VCs
* Differentiation


## Why, When & How of Raising money

**Should you raise? Are you a venture type of business?**

VCs are typically looking for companies which have the potential, ability and market fit to grow and become a $100,000,000 market cap in 8-10 years.
Something founders should keep in mind is that a valuation increase means profit for the VCs but that doesn’t necessarily translate to your profit or the company. Having huge valuations is meaningless if the underlying business is not strong.

**Are you willing to partner?**

As you raise funding, you lose control. It is how this works.


**Do you want to chase a big vision?**

You only get to raise a seed round once. Choose wisely.


**When to raise?**

* Always have at least 6 months of runway.
* Are you ready as a company?
* You have salaries to pay. And not everyone works for stocks.


**How much to raise?**

* Balance between your dilution vs control
* Seed: 20% - 30% dilution
* Series A: 25% dilution
* Series B: 15% dilution


## Art & Science of Valuation

**Science:** 

Valuation of a B2B company dependent on total revenue eg. B2B SaaS companies, valuation = 5 - 15 times their revenue


For B2C companies, valuation depends upon revenue per user, market size, etc.


Reverse calculate the valuation i.e. Valuation = Money you want to raise/% of equity you want to give away. 


**Art:**

https://www.researchgate.net/publication/349533034_WeWork_Case_Study


**Dilution is not always BAD. But try to keep control till at least series B.**

|Label|Pre Money|Post Money|
|---------------|:-------------:|----------:|
|               |*What you want*|*What they want*   |
|Funding Asked  |$1,000,000     |$1,000,000         |
|Valuation      |$1,000,000     |$1,000,000         |
|Investor Equity|9.09%          |10.00%             |
    

* if the ask and equity are fixed.

Say an investor is ready to invest $1,000,000 and wants 10% of your company. 

Pre-money means the company is valued at $10,000,000 before its money comes into the company.

Post money means the company is veiled at $10,000,000 after his money comes into the company.


## How VC works 

How does a VC work? <sup id="two"><a href="#2">[2]</a></sup>

Structure of a VC fund:

* **Limited Partners:** 
   * HNIs/ULtra wealthy individuals
   * They put the money in the fund


* **General Partners:**
   * They manage the money and make investment decisions


* **Principles & Associates:**
   * They are assistants to General partners
   * Scout for/Interact with the new leads
   * Principles have limited power in terms of who gets money, the associates have none. They only scout the probable founders in search of money.


* **Operations Team:**
   * They help portfolio companies, post-investment.


**Life cycle of a VC fund:**


* **Investment Phase:**
   * 0-4 years typically. Fund investing is very active


* **Follow-on Phase:**<sup id="three"><a href="#3">[3]</a></sup>
   * 4-6 years typically. Reinvesting in the winning bets.


* **Fund closing:** 
   * 6-8 years, time to return money to investors.


**Tips to interact with VC:**

* Don’t get stuck with associates beyond the first meeting.
* In most funds the general partner calls the shots, focusing on him will be beneficial. Associates are just there to assist GP.
* GPs generally possess veto power in investment committee meetings. Eg: Vinod Khosla of Khosla Ventures.
* VCs will always be polite about rejection, hence take the hint.

## How VCs think

**Probability & Pattern recognition:**

They assume most will fail. Some will break even. And a handful will be the ones that give 10x returns. 

They look for everything. The moment you enter their offices and till the moment you leave their office.


## Understanding Pattern: Which is the right VC?

Some are sector agnostic. Even then all have a preference, find your niche.


## Before Funding

**Bootstrapping:** 

When a founder cannot or wouldn't secure funding from a VC, he or she has to pay out of their pocket to sustain the business.
Sustain on the bare minimum.
* Consulting/Freelance/Part-time jobs
* Home is your office
* Tight budget
* Built a team/MVP
* Participate in competitions/Demo days
* Government grants, incubation support, monthly stipend, deferred placements


**Accelerators and Incubators:**

They provide:
* Office space
* Network
* Advice
* Community
* Validation


## Process of Raising

1. Preparation
2. Meetings
3. Early Diligence
4. Pitching
5. Term Sheet
6. Due Diligence
7. Closing


**Funding Process overview**

* The most difficult part is “preparation”
* It takes 6 months to typically raise a round
* Seed and Series A process is the same
* Series A stages are more complicated, require extensive work
* Work involved in each stage may vary depending upon the VC


**Preparation and Pitch Deck**

**Prepare yourself:**
* A marriage without divorce
* Very time consuming 
* Founder - Raise! and Close!
* Options if no money comes
* Sufficient runway
* Can business run without you?
* Need to have at least 2 founders
* Fundraising is stressful and frustrating
* A CTO who chose you for stock, not for salary


**Pitch deck:**

* Build your story
* Practice
* Beautiful design
* Don’t try to reinvent the wheel


**Due diligence items**

* Data room all matrices
* Financial Model
* Product demo
* Team prep to make them ready to VC (For Series A especially)


**Pitch deck structure:**

* Cover
* Problem
* Solution
* Product Demo
* Market size
* Business Model
* Competition
* USP/Competitive advantage
* Team
* Traction/Milestones
* Funding information


https://www.slideshare.net/kambosu/uber-pitch-deck

https://slidebean.com/

https://bestpitchdeck.com/

https://www.forbes.com/sites/alejandrocremades/2018/03/02/how-to-create-a-pitch-deck/

https://techcrunch.com/tag/pitch-deck/

**Cover Slide:**

Simple. Clean. Informative.


**Problem Slide:**

* DOs
    * Present your case
    * Clarify the problem
    * Lay down the premise
    * Don’t be boring
* DONTs
    * No complicated jargon
    * No debatable arguments
    * No convoluted claims
    * No lying
    

**Solution Slide:**

* 3 to 4 functions
* Talk about benefits not features
* Need to have one killer line<sup id="four"><a href="#4">[4]</a></sup>
* Avoid jargon at all cost
* Keep it simple


**Product Demo:**

* Aha! moment in 30 sec or less
* Live demo is risky, video is safer
* Add video link inseam of embedding
* Videos should be in clickable format if sent via email


**Market Share slide:**

[Click me.](https://quickbooks.intuit.com/r/growing-a-business/top-down-vs-bottom-up-which-financial-forecasting-model-works-for-you/)


**Business Model:**

* Make or break the presentation
* Has to be simple
* Make rough estimates


**Competition Slide (Use grid format):**

There is an industry-standard. Just follow it.


**Competitive advantage:**

Can be technical


**Go to market slide:**

* How to make it big.
* Fake it till you make it.
* Modify over time as you try and fail
* Believe 100% in your plan


**Team slide:**

* Don't talk about anyone who is not 100% dedicated to your company.
* Founders, follow the rule of 3Hs
   * Hustler CEO
   * Hacker TECHNICAL
   * Hipster MARKETING AND BRANDING


**Traction Slide:**

* It’s the lethal blow at the climax of your presentation
* If you have a growing revenue show only that graph
* While pitching on a stage this can be slid after the intro


**Booking Meetings:**

No Magic:
* Build a network, and build a list of probable investors
   * Identify target (VC, Angles<sup id="five"><a href="#5">[5]</a></sup>)
   * Do research
      * Find out GPs
      * Follow on social media
      * Read Articles
      * Understand the Decision-making process
      * Pattern recognition
   * Gorilla Marketing


**Booking in-person meetings (Decreasing order of effectiveness):**

1. Warm Intro: Get mutual connections to introduce you to them.
2. Events: Demo days, pitching sessions.
3. Cold emails, calls, Twitter, and LinkedIn.
   1. Add tracking tools to your emails.
   2. Share drive links for PPT don’t embed.
   3. Be as discreet as possible, small community.
   4. Send personalised mail, no CC/BCC.
   5. Can add an expiry feature if you have a term sheet with other VC ready.


**Pitching:**

* Introduction, relationships
* Business + Vision
* Interactive, excite them
* Qualify/Unqualify
* Book follow-up meeting
* Keep logs


**Outcomes:**

1. You get destroyed.
2. You are early.
3. Positive.


**Pitching guidelines:**

* DOs:
    * Simple is better
    * Make it like a story
    * PPT should be accessible over a link
    * VCs take a maximum of 4 min to review a deck(if emailing the pitch)
* DONTs: 
    * Making it longer than 15 slides.
    * Don’t overcrowd your slide
    * Don’t bombard with wild projections and technical details
    * No investors will sign NDA to see a pitch


**Term sheet:**

Key terminologies.

* Controlling terms:
    * Board seats
    * Voting rights
    * Vesting of your stocks
    * Termination conditions
    * Liquidation rights
    * Anti-dilution 
    * Drag along clause
* Financing terms:
    * Pre/Post Money valuations
    * Preferred shares
    * Warrants
    * Stocks and convertible notes


**What is a term sheet?**

* Informal proposal to invest in your startup
* Includes key terms
* Few days to 2 weeks to negotiate and sign
* Legally binding? NO!


* Optimise VC meetings line-up to maximise the number of terms sheets simultaneously
* This will boost your valuation and negotiation power
* Immediately consult with your Board, lawyer, and advisors


* Once everything is negotiated, investors and founders will sign the term sheet with closing the deal in 30 - 45 days
* You enter into formal due diligence


* **Make sure:**
   * Reason for Raising.
   * Is this the right partner?
   * Is this the right amount?
   * Will it help chase the dream?
* **Control:**
   * Board seats 
   * Can they replace you?
   * Do you give control in pre-seed, seed, and series A?
   * You must stay in the driver's seat.
* **Dilution:**
   * Try to own as much as possible
   * Negotiate hard on all terms including financial terms
   * Dilution is not bad, remember - It’s better to have a small piece of a bigger pie and a bigger piece of a small pie
* **Due diligence and closing:**<sup id="six"><a href="#6">[6]</a></sup>
   * VCs check your data. Financial, Legal, Tech, Team, Market, Customers.
   * Prep material. Fact checks it.
   * Be transparent.
   * Keep pushing to the closing deadline.
   * Run the legal paperwork in parallel.
   * Get a good lawyer.
   * Make sure you understand the terms.
   * Angle tax compliances.
   * Have a good Lawyer, CA, CS.
________________

<a id="1" href="#one">[1]</a> Bridge round: Say you have raised Series A and are on track to raise Series B in n months but in n-1 months it is seen that the company is very close to its next target but needs to raise some capital to get to the next step. This is where the bridge round comes in. Can be raised at any time. Generally, at the same valuation or in rare cases where the company is in distress the valuation can be lower

<a id="2" href="#two">[2]</a> A VC fund needs to deliver returns to LPs in 7-9 years time7

<a id="3" href="#three">[3]</a> Best not to raise money during this period, seeing as VC will rush you, as they need to give returns to their LPs

<a id="4" href="#four">[4]</a> I got this bruh.

<a id="5" href="#five">[5]</a> Refer to chapter How VCs thinks

<a id="6" href="#six">[6]</a> Good books:

1. Venture deals by Brad Fels and Jason Mendelson
1. Pitch Anything by Oren Klaff
