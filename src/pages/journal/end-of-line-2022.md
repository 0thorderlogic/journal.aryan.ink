---
layout: "../../layouts/BlogPost.astro"
title: "End of line, 2022"
description: "Annual message from Aryan Singh, towards the end of the year."
pubDate: "December 31, 2022"
heroImage: "https://media.aryan.ink/assets/images/end-of-line-01.png"
altImage: "A man staring towards the sky standing at the edge of a cliff"
author: "a115"
authorName: "Aryan Singh"
---

Just as the bell rang, the time before.
Just as he swept her off the floor.
It's that time of the year again, the time to let go of the grudges we hold, the feud with you brothers and sisters. 
It's time to let go of her memory.
A spring in my walk, hope in my eyes.
It's that time of the year again, just as before.
Good Night, Calcutta! 