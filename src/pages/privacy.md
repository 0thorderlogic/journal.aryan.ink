---
layout: "../layouts/Pages.astro"
title: "Privacy Policy"
date: "April 13, 2023"
description: "Privacy policy of this website"
---

I Aryan Singh, (together with my affiliates, “Aryan Singh”, “we”, “I” “our” or “us”) respect your privacy and are strongly committed to keeping secure any information we obtain from you or about you. This Privacy Policy describes our practices concerning Personal Information we collect from or about you when you access this site and its affiliates’ websites (collectively, the “Site”), or use Aryan Singh and its affiliates’ products and services, including application programming interfaces, associated software, tools, developer services, data and documentation (collectively, the “Services”).

This policy applies to all sites mentioned [here](https://directory.aryan.ink).

## Personal Information I collect

I use google analytics to collect some data. 

## How I use personal information

I improve this site. I don't share data with anyone.

## Sharing and disclosure of personal information 

I don't. Any such requests are it from the Government of any country will be ignored.

## How to contact me; Updating your information

Please contact [aoneone5 [at] protonmail [dot] com](mailto:aoneone5@protonmail.com) if you have any questions or concerns not already addressed in this Privacy Policy.
I cannot update your information.

## Children

Don't use my site if you are under 18.

## Links to other websites

The Service may contain links to other websites not operated or controlled by Aryan Singh, including social media services (“Third Party Sites”). The information that you share with Third Party Sites will be governed by the specific privacy policies and terms of service of the Third Party Sites and not by this Privacy Policy. By providing these links we do not imply that we endorse or have reviewed these sites. Please contact the Third Party Sites directly for information on their privacy practices and policies.

## Your choice

You may choose to not use this site, in its current form. You can avoid sending data through google analytics by simpling disabling javascript to run. This action will not break this site. Alternatively, you can use something like [RSS](/rss.xml) to read the articles listed here.


## Changes to the privacy policy

I may change this Privacy Policy at any time. When we do we will post an updated version on this page, unless another type of notice is required by applicable law. By continuing to use our Service or providing us with Personal Information after we have posted an updated Privacy Policy, or notified you by other means, you consent to the revised Privacy Policy.