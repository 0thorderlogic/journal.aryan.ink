import rss from '@astrojs/rss';
import sanitizeHtml from 'sanitize-html';

const postImportResult = import.meta.glob('./journal/**/*.md', { eager: true });
const posts = Object.values(postImportResult);

export const get = () => rss({
	customData: `<language>en-us</language>`,
	title: "Aryan's Journal",
	description: "The blog of Aryan Singh",
	site: import.meta.env.SITE,
	items: posts.map((post) => ({
		link: post.url,
		title: post.frontmatter.title,
		pubDate: post.frontmatter.pubDate,
		description: post.frontmatter.description,
		customData: sanitizeHtml(post.compiledContent()),
  }))
});
