# Aryan's Journal

![Social](https://journal.aryan.ink/social.png)
![Social](https://journal.aryan.ink/social-old.png)

[Visit Site](http://journal.aryan.ink)

Source code, README.
Project documentation: https://docs.astro.build

## Commands you will find useful if you fork this repo!

All commands are run from the root of the project, from a terminal:

| Command                | Action                                             |
| :--------------------- | :------------------------------------------------- |
| `npm install`          | Installs dependencies                              |
| `npm run dev`          | Starts local dev server at `localhost:3000`        |
| `npm run build`        | Build your production site to `./dist/`            |
| `npm run preview`      | Preview your build locally, before deploying       |
| `npm run astro ...`    | Run CLI commands like `astro add`, `astro preview` |
| `npm run astro --help` | Get help using the Astro CLI                       |

## Todo list

All done. I still update things here an there.

## Notice

Hey can you make a search for this static website?<br/>
Help me out. Contact me! Email: aoneone5 [at] protonmail [dot] com

## Post arguments

```
---
layout: "../../layouts/BlogPost.astro"
title: "Title"
description: "Description"
pubDate: "Month Day, Year"
heroImage: "https://media.aryan.ink/assets/images/sample.png"
altImage: "Description of the image"
author: "id"
authorName: "FirstName LastName"
---
```

```
---
layout: "../layouts/Pages.astro"
title: "Title"
date: "Month Day, Year"
description: "Description"
---
```