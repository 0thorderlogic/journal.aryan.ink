import { defineConfig } from 'astro/config';
import partytown from "@astrojs/partytown";
import sitemap from '@astrojs/sitemap';
import robotsTxt from 'astro-robots-txt';
import image from "@astrojs/image";
import compress from "astro-compress";
import prefetch from '@astrojs/prefetch';

export default defineConfig({
  site: 'https://journal.aryan.ink',
  integrations: [
				prefetch(),
				sitemap(),
				robotsTxt(), 
				image({ serviceEntryPoint: '@astrojs/image/sharp'}),
				compress(),
				partytown({config: {forward: ["datalayer.push"],},}),
				]
});